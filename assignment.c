/*for this question I'm assuming that for every Rs.1 that changes in price of ticket that a difference of 4 attendees will take place in accordance with the relationship given*/

#include<stdio.h>

int calcAttendees(int price);
int calcProfit(int price, int attendees);

int main()
{
	int price, attendeeAmount, profit = 0, maxProfit = 0;

	for(price = 15; ;price++)
	{
		attendeeAmount = calcAttendees(price);
		profit = calcProfit(price,attendeeAmount);

		if(profit >= maxProfit)
		{
			maxProfit = profit;
		}
		else
		{
			price = price - 1;
			break;
		}
	}

	printf("The maximum profit is %d when ticket is priced at Rs.%d.00 \n", maxProfit, price);

	return 0;
}

int calcAttendees(int price)
{
	int amount;
	amount = 120 - ((price - 15) * 4);
	return amount;
}

int calcProfit(int price, int attendees)
{
	int profit;
	profit = (price * attendees) - (500 + (attendees * 3));
	return profit;
}
